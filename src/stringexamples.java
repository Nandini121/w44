
public class stringexamples {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		// String examples:
				/// --------------------
				// 1. Create a string
				String name = "peter sigurdson";
				System.out.println(name);
				
				// 2. Number of characters in the string
				int numChars = name.length();
				System.out.println("Number of characters: " + numChars);
				
				// 3. Get a specific character
				char charAtPosition4 = name.charAt(4);
				System.out.println("Char at Pos 4: " + charAtPosition4);
				
				// 4. Get a substring
				String sub = name.substring(0, 5);
				System.out.println("Get substring 1: " + sub);
				
				String sub2 = name.substring(7);
				System.out.println("Get substring 2: " + sub2);
				
				// 5. Check if one string is equal to another
				String a = "emad";
				String b = "mohammad";
				String c = "emad";
					
				// compare emad and mohammad
				if (a.contentEquals(b)) {
					System.out.println("a & b are same!");
				}
				else {
					System.out.println("a & b are NOT same!");
				}
				
				// compare emad and emad
				if (a.contentEquals(c)) {
					System.out.println("a & c are same!");
				}
				else {
					System.out.println("a & c are NOT same!");
				}
				
				// do an OBJECT comparison between a & c
				// compare emad and emad
				if (a == c) {
					System.out.println("a1 & c1 are same!");
				}
				else {
					System.out.println("a1 & c1 are NOT same!");
				}
				
				// 6. Make everything uppercase
				String m = "yelling";
				System.out.println(m.toUpperCase());
				
				String p = "YeLlInnnnnGGGGG";
				System.out.println(p.toUpperCase());
				
				// 7. Make everything lowercase
				String t = "PINEAPPLE";
				System.out.println(t.toLowerCase());
				
				String t2 = "PINNNNNEEEaaappLLEEEE";
				System.out.println(t.toLowerCase());
						
	}

}
